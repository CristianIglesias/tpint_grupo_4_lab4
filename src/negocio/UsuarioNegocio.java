package negocio;
import dao.UsuarioDao;
import entidades.Usuario;
public class UsuarioNegocio{
	
	public Usuario buscar(Usuario usuario) {
		UsuarioDao dao= new UsuarioDao();
		Usuario u= dao.buscar(usuario);
		return u;
	}
	
}
