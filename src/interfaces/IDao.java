package interfaces;
import java.util.ArrayList;


public interface IDao<T> {
	public boolean crear(T objeto);
	public boolean eliminar(T objeto);
	public ArrayList<T> listar();
	public ArrayList<T> buscar(long ID);
	public boolean modificar(T objeto);
}
