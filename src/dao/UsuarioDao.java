package dao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import entidades.Tipo;
import entidades.Usuario;

public class UsuarioDao{
	private static final String buscar = "SELECT * FROM usuarios WHERE usuario=? AND contraseña=?";

	public Usuario buscar(Usuario usuarioParametro) {
		Usuario usuario = null;
		PreparedStatement statement;
		ResultSet resultSet;
		Connection conexion = Conexion.getConexion().getSQLConexion();
		try 
		{
			statement = conexion.prepareStatement(buscar);
			statement.setString(1, usuarioParametro.getUsuario());
			statement.setString(2, usuarioParametro.getContraseña());
			resultSet = statement.executeQuery();
			
			resultSet.next();	
			usuario= getUsuario(resultSet);
		}
		catch (SQLException e) 
		{
			e.printStackTrace();			
		}
		return usuario;
	}
	
	private Usuario getUsuario(ResultSet resultSet) throws SQLException
	{
		long id = resultSet.getLong(1);
		String nombre = resultSet.getString(2);
		String contraseña= resultSet.getString(3);
		int idTipo=resultSet.getInt(4);
		
		TipoDao tipoDao= new TipoDao();
		Tipo tipo= tipoDao.buscar(idTipo);
		
		return new Usuario(id,nombre,contraseña,tipo);
	}
}
