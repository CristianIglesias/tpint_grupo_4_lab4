package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import entidades.Tipo;

public class TipoDao{
	private static final String buscar = "SELECT * FROM tipos_usuario where ID=?";
	
	public TipoDao() {}
	
	public Tipo buscar(int ID) {
		Tipo tipo = null;
		PreparedStatement statement;
		ResultSet resultSet;
		Connection conexion = Conexion.getConexion().getSQLConexion();
		try 
		{
			statement = conexion.prepareStatement(buscar);
			statement.setString(1, Integer.toString(ID));
			resultSet = statement.executeQuery();
			
			resultSet.next();	
			tipo= getTipo(resultSet);
			resultSet.close();
		}
		catch (SQLException e) 
		{
			e.printStackTrace();			
		}
		return tipo;
	}
	
	private Tipo getTipo(ResultSet resultSet) throws SQLException
	{
		int id = resultSet.getInt(1);
		String detalle = resultSet.getString(2);
		return new Tipo(id,detalle);
	}

}
