package entidades;

import negocio.UsuarioNegocio;

public class Usuario{
	private long id;
	private String usuario;
	private String contraseña;
	private Tipo tipo;
	
	public Usuario(long id,String usuario,String contraseña,Tipo tipo) {
		this.id=id;
		this.usuario=usuario;
		this.contraseña=contraseña;
		this.tipo=tipo;
	}
	public Usuario(String usuario,String contraseña) {
		this.usuario=usuario;
		this.contraseña=contraseña;
	}
	
	public Usuario () {
		
	}
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public String getContraseña() {
		return contraseña;
	}
	public void setContraseña(String contraseña) {
		this.contraseña = contraseña;
	}
	public Tipo getTipo() {
		return tipo;
	}
	public void setTipo(Tipo tipo) {
		this.tipo = tipo;
	}
	
	public Usuario login() {
		UsuarioNegocio negocio= new UsuarioNegocio();
		return negocio.buscar(this);
	}
}
