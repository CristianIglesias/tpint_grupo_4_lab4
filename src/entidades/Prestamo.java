package entidades;

import java.util.Date;

public class Prestamo {

	private long id;
	private Date fechaCreacion;
	private Cliente cliente;
	private double importeTotal;
	private double importePedido;
	private double cuotaMensual;
	private int cuotasTotales;
	private int cuotasAbonadas;
	private double saldoPrestamo;
	
	public Prestamo () {
		
	}
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public double getImporteTotal() {
		return importeTotal;
	}

	public void setImporteTotal(double importeTotal) {
		this.importeTotal = importeTotal;
	}

	public double getImportePedido() {
		return importePedido;
	}

	public void setImportePedido(double importePedido) {
		this.importePedido = importePedido;
	}

	public double getCuotaMensual() {
		return cuotaMensual;
	}

	public void setCuotaMensual(double cuotaMensual) {
		this.cuotaMensual = cuotaMensual;
	}

	public int getCuotasTotales() {
		return cuotasTotales;
	}

	public void setCuotasTotales(int cuotasTotales) {
		this.cuotasTotales = cuotasTotales;
	}

	public int getCuotasAbonadas() {
		return cuotasAbonadas;
	}

	public void setCuotasAbonadas(int cuotasAbonadas) {
		this.cuotasAbonadas = cuotasAbonadas;
	}

	public double getSaldoPrestamo() {
		return saldoPrestamo;
	}

	public void setSaldoPrestamo(double saldoPrestamo) {
		this.saldoPrestamo = saldoPrestamo;
	}

	
}
