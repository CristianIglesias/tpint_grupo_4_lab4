package entidades;

import negocio.TipoNegocio;

public class Tipo{
	private int id;
	private String detalle;
	
	public Tipo(int id,String detalle) {
		this.setId(id);
		this.setDetalle(detalle);
	}
	
	public String getDetalle() {
		return detalle;
	}
	public void setDetalle(String detalle) {
		this.detalle = detalle;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public static Tipo buscar(int id) {
		TipoNegocio tipoNegocio= new TipoNegocio();
		return tipoNegocio.buscar(id);
	}
	
}
