package entidades;
import java.util.ArrayList;
import java.util.Date;

public class Cliente extends Usuario {
	
	private String nombre;
	private String apellido;
	private int dni;
	private int cuil;
	private String sexo;
	private String pais;
	private String direccion;
	private String localidad;
	private String provincia;
	private String mail;
	private Date fechaNac;
	private ArrayList<Telefono> telefonos;
	private ArrayList<Cuenta>cuentas;
	private ArrayList<Prestamo>prestamos;
	
	
	public Cliente()
	{
		super();
	}
	public Cliente(String nombre, String apellido, int dni, int cuil, String sexo, String pais, String direccion, String localidad, String provincia, String mail, Date fechaNac, ArrayList<Telefono> telefonos, ArrayList<Cuenta>cuentas,ArrayList<Prestamo>prestamos ){
		
		super();
		this.nombre= nombre;
		this.apellido= apellido;
		this.dni= dni;
		this.cuil=cuil;
		this.sexo=sexo;
		this.pais=pais;
		this.direccion=direccion;
		this.localidad=localidad;
		this.provincia= provincia;
		this.mail=mail;
		this.fechaNac= fechaNac;
		this.setTelefonos(telefonos);
		this.cuentas= cuentas;
		this.prestamos= prestamos;
		
	}
	
	public ArrayList<Cuenta> getCuentas() {
		return cuentas;
	}



	public void setCuentas(ArrayList<Cuenta> cuentas) {
		this.cuentas = cuentas;
	}



	public ArrayList<Prestamo> getPrestamos() {
		return prestamos;
	}



	public void setPrestamos(ArrayList<Prestamo> prestamos) {
		this.prestamos = prestamos;
	}



	public void setSexo(String sexo) {
		this.sexo = sexo;
	}



	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getApellido() {
		return apellido;
	}
	public void setApellido(String apellido) {
		this.apellido = apellido;
	}
	public int getDni() {
		return dni;
	}
	public void setDni(int dni) {
		this.dni = dni;
	}
	public int getCuil() {
		return cuil;
	}
	public void setCuil(int cuil) {
		this.cuil = cuil;
	}
	
	public String getPais() {
		return pais;
	}
	public void setPais(String pais) {
		this.pais = pais;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	public String getLocalidad() {
		return localidad;
	}
	public void setLocalidad(String localidad) {
		this.localidad = localidad;
	}
	public String getProvincia() {
		return provincia;
	}
	public void setProvincia(String provincia) {
		this.provincia = provincia;
	}
	public String getMail() {
		return mail;
	}
	public void setMail(String mail) {
		this.mail = mail;
	}
	
	public Date getFechaNac() {
		return fechaNac;
	}
	public void setFechaNac(Date fechaNac) {
		this.fechaNac = fechaNac;
	}
	
	@Override
	public String toString() {
		return "Cliente [nombre=" + nombre + ", apellido=" + apellido + ", dni=" + dni + ", cuil=" + cuil + ", sexo="
				+ sexo + ", pais=" + pais + ", direccion=" + direccion + ", localidad=" + localidad + ", provincia="
				+ provincia + ", mail=" + mail + ", idUsuario="  + ", fechaNac=" + fechaNac + "]";
	}



	public ArrayList<Telefono> getTelefonos() {
		return telefonos;
	}



	public void setTelefonos(ArrayList<Telefono> telefonos) {
		this.telefonos = telefonos;
	}
    
	

}
