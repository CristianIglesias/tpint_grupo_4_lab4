package servlets;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import entidades.Usuario;
import negocio.UsuarioNegocio;


@WebServlet("/servletUsuario")
public class servletUsuario extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public servletUsuario() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String nombre= request.getParameter("txtUsuario");
		String contraseņa=request.getParameter("txtPassword");
		Usuario usuario=new Usuario(nombre,contraseņa);
		
		UsuarioNegocio negocio= new UsuarioNegocio();
		usuario= negocio.buscar(usuario);
		
		request.setAttribute("usuario", usuario);
		RequestDispatcher rd= request.getRequestDispatcher("/Login.jsp");
		rd.forward(request, response);
		
		//doGet(request, response);
	}

}
