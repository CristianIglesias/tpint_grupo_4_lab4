<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<!--  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" 
integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">-->

<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" 
integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<h1>Bienvenido</h1>
<h3>Menu</h3> 

<div class="row-cols-1 row-cols-md-2 g-4"">
    <div class="col">
<div class="card border-info mb-3" style="max-width: 18rem;">
  <div class="card-header">Mi perfil</div>
  <div class="card-body">
  <a href="ClientePerfil.jsp" class="btn btn-primary">Ingresar</a>
</div>
</div>
 </div>

    <div class="col">
<div class="card border-info mb-3" style="max-width: 18rem;">
  <div class="card-header">Mis cuentas</div>
  <div class="card-body">
  <a href="ClienteCuentas.jsp" class="btn btn-primary">Ingresar</a>
  </div>
</div>
 </div>
  
    <div class="col">
<div class="card border-info mb-3" style="max-width: 18rem;">
  <div class="card-header">Transferencias</div>
  <div class="card-body">
  <a href="ClienteTransferencias.jsp" class="btn btn-primary">Ingresar</a>
  </div>
</div>
 </div>
  
    <div class="col">
<div class="card border-info mb-3" style="max-width: 18rem;">
  <div class="card-header">Solicitar prestamo</div>
  <div class="card-body">
  <a href="ClientePrestamo.jsp" class="btn btn-primary">Ingresar</a>
  </div>
</div>
 </div>

    <div class="col">
<div class="card border-info mb-3" style="max-width: 18rem;">
  <div class="card-header">Pagos</div>
  <div class="card-body">
  <a href="ClientePagos.jsp" class="btn btn-primary">Ingresar</a>
  </div>
</div>
 </div>
</div>

</body>
</html>