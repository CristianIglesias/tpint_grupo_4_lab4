<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<!--  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" 
integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">-->

<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" 
integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<p style="text-align: center" >Cerrar sesion</p>
<h1>Principal Administrador</h1>
<h3>Menu</h3>

<div class="row row-cols-1 row-cols-md-2 g-4">
  <div class="col">
<div class="card border-info mb-3" style="max-width: 18rem;">
  <div class="card-header">ABML Cliente</div>
  <div class="card-body">
    <a href="AdminABMLClientes.jsp" class="btn btn-primary">Ingresar</a>
  </div>
</div>
</div>

<div class="col">
<div class="card border-info mb-3" style="max-width: 18rem;">
  <div class="card-header">ABML Cuentas</div>
  <div class="card-body">
    <a href="AdminABMLCuentas.jsp" class="btn btn-primary">Ingresar</a>
  </div>
</div>
</div>

<div class="col">
<div class="card border-info mb-3" style="max-width: 18rem;">
  <div class="card-header">Autorizacion de Prestamos</div>
  <div class="card-body">
    <a href="AdminPrestamos.jsp" class="btn btn-primary">Ingresar</a>
  </div>
</div>
</div>

<div class="col">
<div class="card border-info mb-3" style="max-width: 18rem;">
  <div class="card-header">Estadistica</div>
  <div class="card-body">
    <a href="AdminReportes.jsp" class="btn btn-primary">Ingresar</a>
  </div>
</div>
</div>
</div>


</body>
</html>