<%@ page import="entidades.Usuario"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<!--  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" 
integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">-->

<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" 
integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>

<h1>Login</h1>
<br>

<form action="servletUsuario" method="post"> 
  Usuario <input type="text" name="txtUsuario"></input>
  <br>
  Password <input type="password" name="txtPassword"></input>
  <br>
  <input type="submit" value="Aceptar" name="btnAceptar">
  <br>
</form>

<%
	Usuario usuario = null;
	if(request.getAttribute("usuario")!=null){
	usuario = (Usuario)request.getAttribute("usuario");
	
  if (usuario.getTipo().getId()== 1){
    	%>
    	<a href="AdminPrincipal.jsp"></a>
    	<% 
    	
	}
    
    else
    {
    	%><a href="ClientePrincipal.jsp"></a>
    	<% 
    } 
	
	%>
  
	<script type="text/javascript">
    alert("Usuario Logueado con exito");
	</script>
	
		<h2>Usuario logeado: <%= usuario.getUsuario() %> de tipo <%= usuario.getTipo().getDetalle() %></h2>
		
	<%}
%>

</body>
</html>